package com.delta.springnotes.models;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    // private fields
    @Id
    @GeneratedValue
    private long id;
    @Column(nullable = false, length =300, unique = true)
    private String username;
    @Column(nullable = false, length = 300, unique = true)
    private String email;
    @Column(nullable = false, length = 300)
    private String password;

    // empty constructor
    public User() {
    }

    // copy constructor
    public User(User copy){
        id = copy.id; // this line is SUPER important! Many things wont work if its absent
        email = copy.email;
        username = copy.username;
        password = copy.password;
    }

    // getters and setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
