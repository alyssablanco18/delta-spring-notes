package com.delta.springnotes.repos;

import com.delta.springnotes.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BookRepository extends JpaRepository<Book, Long> {
    // the following method is equivalent to the built-in 'getOne' method
    @Query("from Book b where b.id like ?1")
    Book getBookById(long id);
}
