package com.delta.springnotes.controllers;

import com.delta.springnotes.models.Book;
import com.delta.springnotes.repos.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class BookController {

    // spring boots dependency injection
    private final BookRepository bookRepo;

    // constructor
    public BookController(BookRepository bookRepo) {
        this.bookRepo = bookRepo;
    }

    // get all books and display them on books/index view
    @GetMapping("/books")
    public String showBooks(Model model) {
        List<Book> bookList = bookRepo.findAll();
        model.addAttribute("noBooksFound", bookList.size() == 0);
        model.addAttribute("books", bookList);
        return "Books/index"; // name of the view books/index
    }

    // add a book form
    @GetMapping("/books/create")
    public String showForm(Model model) {
        model.addAttribute("newBook", new Book());
        return "Books/create";
    }

    @PostMapping("/books/create")
    public String createBook(@ModelAttribute Book bookToCreate) {
        // save the bookToCreate parameter
        bookRepo.save(bookToCreate);
        // redirect user to the list of books
        return "redirect:/books/";
    }

    // controller method to display an individual book
    @GetMapping("/books/{id}")
    public String showBook(@PathVariable long id, Model model) {
        Book book = bookRepo.getOne(id); // getting an object of Book based on the id

        model.addAttribute("showBook", book);

        return "Books/show";
    }

    // controller method that will allow the user to edit
    @GetMapping("/books/{id}/edit")
    public String showEdit(@PathVariable long id, Model model) {
        // find the book object with the passed in id
        Book bookToEdit = bookRepo.getOne(id);
        model.addAttribute("editBook", bookToEdit);

        return "Books/edit";

    }

    @PostMapping("/books/{id}/edit")
    public String updateBook(@PathVariable long id,
                             @RequestParam(name = "title") String title,
                             @RequestParam(name = "author") String author,
                             @RequestParam(name = "genre") String genre,
                             @RequestParam(name = "releaseDate") String releaseDate)
    {
        // find the book with the passed in id
        Book foundBook = bookRepo.getBookById(id);

        // update the books title,author,genre,release
        foundBook.setTitle(title);
        foundBook.setAuthor(author);
        foundBook.setGenre(genre);
        foundBook.setReleaseDate(releaseDate);

        // save the new books data changes
        bookRepo.save(foundBook);

        // redirect the user to the url that contains the list of books
        return "redirect:/books/";
    }

    // controller method that will allow the user to delete objects of our class
    @PostMapping("/books/{id}/delete")
    public String delete(@PathVariable long id) {
        // access the deleteBYId method from repository
        bookRepo.deleteById(id);

        // redirect the user to the url containing the list of ads
        return "redirect:/books/";
    }


}
