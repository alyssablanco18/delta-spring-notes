package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MathController {

    // add method
    @GetMapping(path = "/add/{num1}/and/{num2}")
    @ResponseBody
    public String add(@PathVariable int num1, @PathVariable int num2) {
        return num1 + " plus " + num2 + " is " + (num1 + num2);
    }

    // another add method
    @GetMapping("/add/{num1}/{num2}")
    @ResponseBody
    public int addNumbers(@PathVariable int num1, @PathVariable int num2) {
        return num1 + num2;
    }


    // subtract method
    @RequestMapping(path = "/subtract/{num1}/{num2}", method = RequestMethod.GET)
    @ResponseBody
    public int subtract(@PathVariable int num1, @PathVariable int num2) {
        return num1 - num2;
    }

    // multiply method
    @GetMapping("/multiply/{num1}/{num2}")
    @ResponseBody
    public int multiply(@PathVariable int num1, @PathVariable int num2) {
        return num1 * num2;
    }



    // divide method
    @GetMapping("/divide/{number1}/{number2}")
    @ResponseBody
    public int divide(@PathVariable int number1, @PathVariable int number2) {
        return number1 / number2;
    }

}
