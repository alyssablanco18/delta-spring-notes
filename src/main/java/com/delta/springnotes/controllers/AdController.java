package com.delta.springnotes.controllers;

import com.delta.springnotes.models.Ad;
import com.delta.springnotes.repos.AdRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class AdController {

    // spring boot's dependency injection
    private final AdRepository adRepo;

    // constructor
    public AdController(AdRepository adRepo) {
        this.adRepo = adRepo;
    }

    // get all of the ads and display them on ads/index view
    @GetMapping("/ads")
    public String showAds(Model model) {
        List<Ad> adList = adRepo.findAll();

        model.addAttribute("noAdsFound", adList.size() == 0);
        model.addAttribute("ads", adList);

        return "ads/index"; // name of the view 'ads/index'
    }


    // show the create / add and ad form
    @GetMapping("/ads/create")
    public String showForm(Model model) {
        model.addAttribute("newAd", new Ad());

        // show the user the create view
        return "ads/create";
    }

    @PostMapping("/ads/create")
    public String createAd(@ModelAttribute Ad adToCreate) {

        // save the adToCreate parameter
        adRepo.save(adToCreate);

        // redirect the use to the list of ads -> /ads
        return "redirect:/ads/";
    }

    // controller method to display an individual ad
    @GetMapping("/ads/{id}")
    public String showAd(@PathVariable long id, Model model) {
        Ad ad = adRepo.getOne(id); // getting an object of Ad based on the id

        model.addAttribute("showAd", ad);

        return "ads/show";
    }

    // controller method that will allow for our user to edit
    @GetMapping("/ads/{id}/edit")
    public String showEdit(@PathVariable long id, Model model) {
        // find the ad object with the passed in id
        Ad adToEdit = adRepo.getOne(id);
        model.addAttribute("editAd", adToEdit);

        return "ads/edit";
    }

    @PostMapping("/ads/{id}/edit")
    public String updateAd(@PathVariable long id,
                           @RequestParam(name = "title") String title,
                           @RequestParam(name = "description") String description)
    {
        // find the ad with the passed in id
        Ad foundAd = adRepo.getAdById(id); // NOTE: in mySQL -> SELECT * FROM ads WHERE id = ?

        // update the ad's title and description
        foundAd.setTitle(title);
        foundAd.setDescription(description);

        // save the new ad's data changes
        adRepo.save(foundAd);

        // redirect the user to the url that contains the list of ads
        return "redirect:/ads/";

    }

    // controller method that will allow the user to delete objects of our class
    @PostMapping("/ads/{id}/delete")
    public String delete(@PathVariable long id) {
        // access the deletebyId() method from the repository
        adRepo.deleteById(id);

        // redirect the user to the url containing the list of ads
        return "redirect:/ads/";
    }
}
