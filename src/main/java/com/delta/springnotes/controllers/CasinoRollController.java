package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Random;

@Controller
public class CasinoRollController {

    @GetMapping("/casino-roll")
    public String showCasinoRoll() {
        return "guess-roll-view";
    }

    @GetMapping("/casino-roll/{num}")
    public String userGuessedNumber(@PathVariable int num, Model model) {

        int correctNumber = (int) (Math.random() * (5) + 1);

        String message;

        if (correctNumber == num) {
            message =  "YOU GUESSED CORRECTLY, yayy";
        }
        else {
            message = "you guessed wrong :(";
        }

        model.addAttribute("showcaseMessage",message);

        return "guess-roll-view";
    }


}
